import { functions } from './functions';

export module PopupModule {
  export interface $popupEventOptions {
    $target?: string,
    $content?: string
  }

  const defaults: $popupEventOptions = {
    $target: '',
    $content: ''
  };

  export let $popupCloseFlg: boolean = false;

  export class Service {
    private static $popupElementId: string = 'js__popupBox';

    constructor(language?: string) {
      document.addEventListener('readCompleteAction', () => {
      });
    }

    /**
     * クローズボタン
     */
    public static popupClose($args: $popupEventOptions = {}) {
      document.addEventListener('click', function ($event) {
        if ($event !== null && $event.target && document.body.classList.contains('js__popupOpen')) {
          const $contentWrapClass: string = '.' + Service.$popupElementId + ' .' + Service.$popupElementId + 'ContentWrap';
          const $contentWrapElement: HTMLElement | null = document.querySelector($contentWrapClass);
          const $closeclass: string = Service.$popupElementId + 'CloseButtonLink';

          let $elements: NodeListOf<Element>;
          if ($args.$target) {
            $elements = document.querySelectorAll($args.$target) as NodeListOf<Element>;
          } else {
            $elements = document.getElementsByClassName('js__popup') as unknown as NodeListOf<Element>;
          }

          if ($contentWrapElement) {
            if (($event.target as Element).classList.contains($closeclass)) {
              // クローズボタンをクリックした時
              $event.preventDefault();
              const $targets = document.querySelectorAll('.js__popupOpen');

              if ($targets.length) {
                $targets.forEach($target => {
                  $target.classList.remove('js__popupOpen');
                });
              }
            } else {
              // コンテンツ外をクリックした時
              $elements.forEach($element => {
                if (!$element.contains(($event.target as Element))) {
                  if (!$contentWrapElement.contains(($event.target as Element))) {
                    const $targets = document.querySelectorAll('.js__popupOpen');
                    if ($targets.length) {
                      $event.preventDefault();
                      $targets.forEach($target => {
                        $target.classList.remove('js__popupOpen');
                      });
                    }
                  }
                }
              });
            }
          }
        }
      }, false);
    }

    /**
     * ポップアップイベント
     */
    public static popupEvent($args: $popupEventOptions = {}) {
      let $elements: NodeListOf<Element> | HTMLCollectionOf<Element>;
      if ($args.$target) {
        $elements = document.querySelectorAll($args.$target);
      } else {
        $elements = document.getElementsByClassName('js__popup');
      }
      if (!PopupModule.$popupCloseFlg) {
        Service.popupClose($args);
        PopupModule.$popupCloseFlg = true;
      }
      if ($elements && $elements.length) {
        for (let $i = 0; $i < $elements.length; $i++) {
          const $unique_id = functions.createUniqueID();
          let $element = $elements[$i];
          $element.setAttribute('data-popupid', $unique_id);
          $element.addEventListener('click', ($event) => {
            let $target: string | null = $element.getAttribute('data-target');
            let $href: string | null = $element.getAttribute('href');
            if (!$target && $href) {
              $target = $href;
            }
            if ($target) {
              $event.preventDefault();
              let $targetElement: HTMLElement | null;
              if ($args.$content) {
                $targetElement = document.querySelector($args.$content) as HTMLElement;
              } else {
                $targetElement = document.querySelector($target) as HTMLElement;
              }
              if ($targetElement) {
                this.popupInsertContent($targetElement, $unique_id);
              }
            }
          });
        }
      }
    }

    /**
     * ポップアップの内容を書き換える
     */
    public static popupInsertContent($targetElement: HTMLElement, $unique_id: string) {
      const $popupElement: HTMLElement = this.popupCreate($targetElement, $unique_id);
      $popupElement.classList.add('js__popupOpen');
      document.body.classList.add('js__popupOpen');
    }

    /**
     * ポップアップ画面生成
     */
    public static popupCreate($targetElement: HTMLElement, $unique_id: string) {
      // ポップアップ画面を作成
      const $popup_id: string = Service.$popupElementId + $unique_id;
      let $popupElement: HTMLElement | null = document.getElementById($popup_id);
      if (!$popupElement) {
        // ポップアップ画面インナー
        let $popupElementInner: HTMLElement = document.createElement('div');
        $popupElementInner.id = $popup_id + 'Inner';
        $popupElementInner.className = Service.$popupElementId + 'Inner';

        // ポップアップ画面コンテントとポップアップ画面クローズボタンを入れるelement
        let $popupElementContentWrap: HTMLElement = document.createElement('div');
        $popupElementContentWrap.id = $popup_id + 'ContentWrap';
        $popupElementContentWrap.className = Service.$popupElementId + 'ContentWrap';

        // ポップアップ画面クローズボタン
        let $popupElementCloseButton: HTMLElement = document.createElement('div');
        let $popupElementCloseButtonLink: HTMLElement = document.createElement('a');
        $popupElementCloseButton.id = $popup_id + 'CloseButton';
        $popupElementCloseButton.className = Service.$popupElementId + 'CloseButton';
        $popupElementCloseButtonLink.id = $popup_id + 'CloseButtonLink';
        $popupElementCloseButtonLink.className = Service.$popupElementId + 'CloseButtonLink';
        $popupElementCloseButtonLink.setAttribute('href', '#');
        $popupElementCloseButtonLink.innerHTML = '<span>Close</span>';

        // ポップアップ画面コンテント
        const $targetElementClone: HTMLElement = $targetElement.cloneNode(true) as HTMLElement;
        let $popupElementContent: HTMLElement = document.createElement('div');
        $popupElementContent.id = $popup_id + 'Content';
        $popupElementContent.className = Service.$popupElementId + 'Content';

        // ポップアップ画面
        $popupElement = document.createElement('div');
        $popupElement.id = $popup_id;
        $popupElement.className = Service.$popupElementId;

        // ポップアップ画面生成
        $popupElementCloseButton.appendChild($popupElementCloseButtonLink);
        $popupElementContentWrap.appendChild($popupElementCloseButton);
        $popupElementContent.appendChild($targetElementClone);
        $popupElementContentWrap.appendChild($popupElementContent);
        $popupElementInner.appendChild($popupElementContentWrap);
        $popupElement.appendChild($popupElementInner);
        if (document.getElementsByTagName('body')) {
          document.body.appendChild($popupElement);
        }
      }
      return $popupElement;
    }

  }
}