export namespace Ease {
  export const EaseFunction = {
    linear: (currentTime: number, startValue: number, changeValue: number, duration: number) => {
      return changeValue * currentTime / duration + startValue;
    },
    easeInCubic: (currentTime: number, startValue: number, changeValue: number, duration: number) => {
      currentTime /= duration;
      return changeValue * currentTime * currentTime * currentTime + startValue;
    },
    easeOutCubic: (currentTime: number, startValue: number, changeValue: number, duration: number) => {
      currentTime /= duration;
      currentTime--;
      return changeValue * (currentTime * currentTime * currentTime + 1) + startValue;
    },
    easeInExpo: (currentTime: number, startValue: number, changeValue: number, duration: number) => {
      return changeValue * Math.pow(2, 10 * (currentTime / duration - 1)) + startValue;
    },
    easeOutExpo: (currentTime: number, startValue: number, changeValue: number, duration: number) => {
      return changeValue * (-1 * Math.pow(2, -10 * currentTime / duration) + 1) + startValue;
    },
    easeInOutSine: (currentTime: number, startValue: number, changeValue: number, duration: number) => {
      return changeValue * (-1 * Math.pow(2, -10 * currentTime / duration) + 1) + startValue;
    },
  }
}