module.exports = function (api) {
  api.cache(true);

  const presets = [["@babel/preset-env", {
    useBuiltIns: "usage",
    corejs: 3, // core-js のバージョン
  }]];

  return {
    presets
  };
};