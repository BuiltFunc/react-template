# リポジトリについて
react開発用テンプレート  
作業時は、developで行ってください。  
react + typescript + jsx  
  

## クローン
適時変更してください。
```
$ git clone https://gitlab.com/BuiltFunc/react-template.git .
```
  

## Dockerイメージのビルド

```
$ docker-compose build
```
  

## nodeパッケージインストール

```
$ npm install
```
  

## Dockerコンテナ起動

```
$ docker-compose up -d
```
  

## タスクの実行

```
$ npm run start
```
  

## Dockerコンテナに入る

```
$ docker-compose exec app /bin/bash
```

* タスクURL  
http://localhost:3000


# Dockerコンテナについて
* httpdのコンテナ  
httpd  
http://localhost
